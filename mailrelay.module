<?php
/**
 * @file
 * Provide API integration with the Mailrelay email marketing platform.
 */

/**
 * Implements hook_permission().
 */
function mailrelay_permission() {
  return array(
    'administer mailrelay' => array(
      'title' => t('Access Mailrelay settings'),
      'description' => t('Allow users to access Mailrelay settings'),
    ),
    'administer mailrelay_newsletterlist' => array(
      'title' => t('Access Mailrelay newsletter list'),
      'description' => t('Allow users to access Mailrelay newsletter list'),
    ),
    'administer mailrelay_statistics' => array(
      'title' => t('Access Mailrelay statistics'),
      'description' => t('Allow users to access Mailrelay statistics'),
    ),
    'administer mailrelay_subscribers' => array(
      'title' => t('Access Mailrelay subscribers'),
      'description' => t('Allow users to access Mailrelay subscribers'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function mailrelay_menu() {
  $items = array();
  $items['admin/config/services/mailrelay'] = array(
    'title' => 'Mailrelay',
    'description' => 'Configures the data from Mailrelay settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('mailrelay_admin'),
    'access arguments' => array('administer mailrelay'),
    'type' => MENU_NORMAL_ITEM,
  );
  $items['admin/config/services/mailrelay/settings'] = array(
    'title' => 'Settings',
    'weight' => 0,
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );

  return $items;
}

/**
 * Returns form items for the mailrelay api settings page.
 */
function mailrelay_admin($form, &$form_state) {
  $form = array();
  $form['mailrelay_hostname'] = array(
    '#type' => 'textfield',
    '#title' => t('Hostname for Mailrelay'),
    '#default_value' => variable_get('mailrelay_hostname', ''),
    '#size' => 40,
    '#maxlength' => 255,
    '#description' => t('Hostname to connect Mailrelay API.'),
    '#required' => TRUE,
  );
  $form['mailrelay_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('API key'),
    '#default_value' => variable_get('mailrelay_api_key', ''),
    '#size' => 50,
    '#maxlength' => 255,
    '#description' => t('The API key generated for our Mailrelay account.'),
    '#required' => TRUE,
  );

  $form['#validate'][] = 'mailrelay_admin_validate';

  return system_settings_form($form);
}

/**
 * Validator for the mailrelay_admin() form.
 */
function mailrelay_admin_validate($form, &$form_state) {
  $hostname = trim($form_state['values']['mailrelay_hostname']);
  $api_key = trim($form_state['values']['mailrelay_api_key']);

  // Validate the API settings with getSubscribers.
  if ($hostname && $api_key) {
    $key = $hostname . $api_key;
    $old_key = variable_get('mailrelay_hostname', '');
    $old_key .= variable_get('mailrelay_api_key', '');

    // Return if no changes.
    if ($key == $old_key) {
      return;
    }

    $params = array(
      'hostname' => $hostname,
      'apiKey' => $api_key,
    );

    $result = mailrelay_api('getSubscribers', $params);
    if ($result === FALSE) {
      form_set_error('', t('Invalid hostname or api_key.'));
    }
  }
}

/**
 * Load the mailrelay api settings from variables.
 *
 * @return array
 *   API configuration of mailrelay.
 */
function mailrelay_api_config() {
  $config = array();

  $hostname = variable_get('mailrelay_hostname', '');
  $api_key = variable_get('mailrelay_api_key', '');

  if ($hostname && $api_key) {
    $config = array(
      'hostname' => $hostname,
      'apiKey' => $api_key,
    );
  }

  return $config;
}

/**
 * Check the mailrelay api settings.
 */
function mailrelay_api_config_check() {
  $config = mailrelay_api_config();
  if (empty($config)) {
    drupal_set_message(t('Mailrelay has not been properly configured. It will not work until you finish configuration.'), 'error');
    drupal_goto('admin/config/services/mailrelay');
  }
}

/**
 * Common function for api of Mailrelay.
 */
function mailrelay_api($function, $params = array()) {
  $config = mailrelay_api_config();
  $params += $config;
  $params['function'] = $function;
  // Return FALSE if api account not set without debug mode.
  if (!isset($params['hostname']) || !isset($params['apiKey'])) {
    return FALSE;
  }
  $api_url = 'https://' . $params['hostname'] . '/ccm/admin/api/version/2/&type=json';
  unset($params['hostname']);
  $postfields = http_build_query($params);
  // Try to get date with API.
  $curl = curl_init($api_url);
  curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($curl, CURLOPT_POST, TRUE);
  curl_setopt($curl, CURLOPT_POSTFIELDS, $postfields);
  curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, TRUE);
  curl_setopt($curl, CURLOPT_SSLVERSION, 3);
  $headers = array('X-Request-Origin: Drupal7');
  curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);

  $json_result = curl_exec($curl);
  $result = json_decode($json_result);

  if (!$result || $result->status == 0) {
    watchdog('mailrelay', 'Error message: [%error], parameters: [%parameters]', array(
      '%error' => isset($result->error) ? $result->error : 'NULL',
      '%parameters' => $postfields), WATCHDOG_ERROR);
    return FALSE;
  }

  return $result;
}
