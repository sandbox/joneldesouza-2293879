CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Features
 * Requirements
 * Installation
 * Configuration
 * Author

INTRODUCTION
------------
Provides API integration with the Mailrelay API

FEATURES
--------
- List of sent newsletters
- Statistics of newsletters with graphics
- Block with Form Signup
- List of subscribers

REQUIREMENTS
------------
* Mailrelay user account [1].
* The php-curl library [2] is required for Mailrelay API to function.

1. http://mailrelay.com/en
2. http://php.net/manual/en/curl.setup.php

INSTALLATION
------------
 * Install as you would normally install a contributed drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.

CONFIGURATION
-------------
1. Generate a new API Key. Sign in on Mailrelay admin panel. Access the Menu
   "Settings > API Access" and click on the link "Generate new API Key"
2. Got to the "Admin » Config » Mailrelay" and enter the Hostname of your
   Mailrelay and your api key generated.

AUTHOR
-----------
 * The module Mailrelay API is developed by Jonel de Souza Nogueira
