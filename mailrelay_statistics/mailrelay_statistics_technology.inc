<?php
/**
 * @file
 * The functions for the Mailrelay Statistics Technology module.
 */

/**
 * Return page callback in mailrelay_statistics_menu().
 */
function mailrelay_statistics_page_technology() {
  $form = drupal_get_form('mailrelay_statistics_form');
  $output = drupal_render($form);
  $output .= mailrelay_statistics_page_technology_detail();
  return $output;
}

/**
 * Get getGeoClicks in Mailrelay API.
 */
function mailrelay_statistics_page_technology_detail() {
  $output = '';
  $mid = variable_get('mailrelay_statistics_value', arg(6));
  if ($mid) {
    // ClicksInfo.
    $statistics = mailrelay_statistics_get_clicks_info($mid);
    $browser = array();
    $os = array();
    $screen_resolution = array();
    foreach ($statistics as $value) {
      $browser[] = $value->browser;
      $os[] = $value->os;
      $screen_resolution[] = $value->screen_width . 'x' . $value->screen_height;
    }
    $count_browser = array_count_values($browser);
    $count_os = array_count_values($os);
    $count_screen = array_count_values($screen_resolution);
    $rows_browser = array();
    foreach ($count_browser as $browser_key => $browser_value) {
      $rows_browser[] = array($browser_key, $browser_value);
    }
    $rows_os = array();
    foreach ($count_os as $os_key => $os_value) {
      $rows_os[] = array($os_key, $os_value);
    }
    $rows_screen = array();
    foreach ($count_screen as $screen_key => $screen_value) {
      $rows_screen[] = array($screen_key, $screen_value);
    }

    $settings = array(
      'mailrelay_statistics' => array(
        'pie_browser' => $count_browser,
        'pie_os' => $count_os,
        'pie_screen' => $count_screen,
        'table_browser' => $rows_browser,
        'table_os' => $rows_os,
        'table_screen' => $rows_screen,
      ),
    );
    $tabs = '(function ($) {$(document).ready(function(){$("#technology").tabs();});})(jQuery);';
    $attached = array(
      'library' => array(
        array('system', 'ui.tabs'),
      ),
      'js' => array(
        array(
          'data' => 'https://www.google.com/jsapi',
          'type' => 'external',
        ),
        drupal_get_path('module', 'mailrelay_statistics') . '/js/statistic_technology.js',
        array(
          'data' => $settings,
          'type' => 'setting',
        ),
        array(
          'data' => $tabs,
          'type' => 'inline',
        ),
      ),
    );

    $item_list = array(
      '<a href="#technology_browser">' . t('Browser') . '</a>',
      '<a href="#technology_os">' . t('Operating system') . '</a>',
      '<a href="#technology_screen">' . t('Screen Resolution') . '</a>',
    );

    $build = array(
      '#attached' => $attached,
      '#prefix' => '<div id="technology">',
      '#suffix' => '</div>',
      'tabs' => array(
        '#theme' => 'item_list',
        '#items' => $item_list,
      ),
      'browser' => array(
        '#type' => 'markup',
        '#prefix' => '<div id="technology_browser">',
        '#markup' => '<h2>' . t('Browser') . '</h2>',
        '#suffix' => '</div>',
      ),
      'os' => array(
        '#type' => 'markup',
        '#prefix' => '<div id="technology_os">',
        '#markup' => '<h2>' . t('Operating system') . '</h2>',
        '#suffix' => '</div>',
      ),
      'screen' => array(
        '#type' => 'markup',
        '#prefix' => '<div id="technology_screen">',
        '#markup' => '<h2>' . t('Screen Resolution') . '</h2>',
        '#suffix' => '</div>',
      ),
    );

    $output = drupal_render($build);
  }

  return $output;
}
