<?php
/**
 * @file
 * The functions for the Mailrelay Statistics Location module.
 */

/**
 * Return page callback in mailrelay_statistics_menu().
 */
function mailrelay_statistics_page_location() {
  $form = drupal_get_form('mailrelay_statistics_form');
  $output = drupal_render($form);
  $output .= mailrelay_statistics_page_location_detail();
  return $output;
}

/**
 * Get getGeoClicks & getGeoViews in Mailrelay API.
 */
function mailrelay_statistics_page_location_detail() {
  $output = '';
  $mid = variable_get('mailrelay_statistics_value', arg(6));
  if ($mid) {
    $mailrelay_statistics = array();
    // ClicksInfo.
    $statistics = mailrelay_statistics_get_clicks_info($mid);
    $stat = mailrelay_statistics_page_location_process($statistics);
    $mailrelay_statistics['clicks_country'] = $stat['country'];
    $mailrelay_statistics['clicks_table'] = $stat['table'];
    // ImpressionsInfo.
    $statistics = mailrelay_statistics_get_impressions_info($mid);
    $stat = mailrelay_statistics_page_location_process($statistics);
    $mailrelay_statistics['views_country'] = $stat['country'];
    $mailrelay_statistics['views_table'] = $stat['table'];

    $settings = array('mailrelay_statistics' => $mailrelay_statistics);
    $tabs = '(function ($) {$(document).ready(function(){$("#location").tabs();});})(jQuery);';
    $attached = array(
      'library' => array(
        array('system', 'ui.tabs'),
      ),
      'js' => array(
        array(
          'data' => 'https://www.google.com/jsapi',
          'type' => 'external',
        ),
        drupal_get_path('module', 'mailrelay_statistics') . '/js/statistic_location.js',
        array(
          'data' => $settings,
          'type' => 'setting',
        ),
        array(
          'data' => $tabs,
          'type' => 'inline',
        ),
      ),
    );

    $item_list = array(
      '<a href="#location_clicks">' . t('Clicks') . '</a>',
      '<a href="#location_views">' . t('Impressions') . '</a>',
    );

    $build = array(
      '#attached' => $attached,
      '#prefix' => '<div id="location">',
      '#suffix' => '</div>',
      'tabs' => array(
        '#theme' => 'item_list',
        '#items' => $item_list,
      ),
      'clicks' => array(
        '#type' => 'markup',
        '#prefix' => '<div id="location_clicks">',
        '#markup' => '<h2>' . t('Clicks') . '</h2>',
        '#suffix' => '</div>',
      ),
      'views' => array(
        '#type' => 'markup',
        '#prefix' => '<div id="location_views">',
        '#markup' => '<h2>' . t('Impressions') . '</h2>',
        '#suffix' => '</div>',
      ),
    );

    $output = drupal_render($build);
  }

  return $output;
}

/**
 * Process statistics data.
 */
function mailrelay_statistics_page_location_process($statistics) {
  $options = $country = $table_rows = array();
  foreach ($statistics as $key => $value) {
    $location = explode(' - ', $value->location);
    $city = $location[0];
    $state = $location[1];
    $options[] = $city . ', ' . $state . ', ' . $value->country;
    $country[] = $value->country;
  }
  $count_table = array_count_values($options);
  $count_country = array_count_values($country);

  foreach ($count_table as $key => $value) {
    $keys = explode(', ', $key);
    $table_rows[] = array($keys[0], $keys[1], $keys[2], $value);
  }

  return array(
    'country' => $count_country,
    'table' => $table_rows,
  );
}
