<?php
/**
 * @file
 * The functions for the Mailrelay Statistics module.
 */

/**
 * Implements hook_menu().
 */
function mailrelay_statistics_menu() {
  $items = array();
  $items['admin/config/services/mailrelay/statistics'] = array(
    'title' => 'Statistics',
    'page callback' => 'mailrelay_statistics_page',
    'access arguments' => array('administer mailrelay_statistics'),
    'weight' => 2,
    'type' => MENU_LOCAL_TASK,
    'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
  );
  $items['admin/config/services/mailrelay/statistics/general'] = array(
    'title' => 'General',
    'page callback' => 'mailrelay_statistics_page',
    'access arguments' => array('administer mailrelay_statistics'),
    'weight' => 1,
    'type' => MENU_DEFAULT_LOCAL_TASK,
  );
  $items['admin/config/services/mailrelay/statistics/location'] = array(
    'title' => 'Location',
    'page callback' => 'mailrelay_statistics_page_location',
    'access arguments' => array('administer mailrelay_statistics'),
    'weight' => 2,
    'type' => MENU_LOCAL_TASK,
    'file' => 'mailrelay_statistics_location.inc',
  );
  $items['admin/config/services/mailrelay/statistics/technology'] = array(
    'title' => 'Technology',
    'page callback' => 'mailrelay_statistics_page_technology',
    'access arguments' => array('administer mailrelay_statistics'),
    'weight' => 3,
    'type' => MENU_LOCAL_TASK,
    'file' => 'mailrelay_statistics_technology.inc',
  );

  return $items;
}

/**
 * The Statistics form.
 */
function mailrelay_statistics_form($form, &$form_state) {
  $mailinglists = mailrelay_newsletterslist_get_mailing_lists_data();
  $options = array();
  foreach ($mailinglists as $row) {
    $options[$row->id] = $row->subject;
  }
  if (!empty($options)) {
    $path = current_path();
    preg_match("/[^\/]+$/", $path, $matches);
    $lastword = $matches[0];
    if (is_numeric($lastword)) {
      $mid = $lastword;
    }
    else {
      $mid = variable_get('mailrelay_statistics_value', '');
    }

    $form['mailinglists'] = array(
      '#title' => t('Newsletters List'),
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => $mid,
      '#required' => TRUE,
    );
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Submit'),
    );
  }

  return $form;
}

/**
 * Form submission handler for mailrelay_signups_form().
 */
function mailrelay_statistics_form_submit($form, &$form_state) {
  variable_set('mailrelay_statistics_value', $form_state['values']['mailinglists']);
}

/**
 * Get getStats in Mailrelay API.
 */
function mailrelay_statistics_get_stats($id) {
  $data = array();
  $result = mailrelay_api('getStats', array('id' => $id));
  if ($result) {
    $data = $result->data;
  }
  return $data;
}

/**
 * Get getClicksInfo in Mailrelay API.
 */
function mailrelay_statistics_get_clicks_info($id) {
  $data = array();
  $result = mailrelay_api('getClicksInfo', array('id' => $id));
  if ($result) {
    $data = $result->data;
  }
  return $data;
}

/**
 * Get getImpressionsInfo in Mailrelay API.
 */
function mailrelay_statistics_get_impressions_info($id) {
  $data = array();
  $result = mailrelay_api('getImpressionsInfo', array('id' => $id));
  if ($result) {
    $data = $result->data;
  }
  return $data;
}

/**
 * Return page callback in mailrelay_statistics_menu().
 */
function mailrelay_statistics_page() {
  mailrelay_api_config_check();
  $render_form = drupal_get_form('mailrelay_statistics_form');
  $mid = variable_get('mailrelay_statistics_value', arg(5));
  if ($mid) {
    $statistics = mailrelay_statistics_get_stats($mid);
    $rows = $gs_type = $gs_quantity = array();

    foreach ($statistics as $key => $value) {
      $key = ucwords(str_replace('_', ' ', $key));
      $rows[] = array($key, $value);
      $gs_type[] = array($key);
      $gs_quantity[] = array($value);
    }

    $settings = array(
      'mailrelay_statistics' => array(
        'gs_type' => $gs_type,
        'gs_quantity' => $gs_quantity,
        'gs_table' => $rows,
      ),
    );

    $attached = array(
      'js' => array(
        array(
          'data' => 'https://www.google.com/jsapi',
          'type' => 'external',
        ),
        drupal_get_path('module', 'mailrelay_statistics') . '/js/statistic_general.js',
        array(
          'data' => $settings,
          'type' => 'setting',
        ),
      ),
    );

    $data = array(
      '#type' => 'markup',
      '#prefix' => '<div id="general">',
      '#markup' => '<h2>' . t('General') . '</h2>',
      '#suffix' => '</div>',
    );
    $output = array($render_form, $data);
    $output['#attached'] = $attached;
    return $output;
  }

  return $render_form;
}
